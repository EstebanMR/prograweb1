<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('user_model');
		$this->load->library('form_validation');
		$this->load->helper('url', 'form');
	}	
	
	public function index()
	{		
		$data['users']=$this->user_model->getAll();
        $this->load->view('user', $data);
	}

	public function insert(){
         
        if($this->input->post("enviar")){

            $add=$this->user_model->insert(
                $this->input->post("nombre"),
                $this->input->post("usuario")
                );
        
            if($add==true){
                redirect('user');
            }else{
            }
         
        }
    }

}