<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<title>CRUD with CodeIgniter</title>

	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body>

<div id="container">
	<h1>Welcome to the CRUD with CodeIgniter!</h1>

	<div id="body">
		<div class="container">
			<div class="row">
				<div class="col-4">
					<?php
						echo form_open('user/add');
						echo '<div class="form-group">';
						echo form_label('Nombre', 'nombre');
						echo form_input(['class' => 'form-control' ,'name' => 'nombre']);
						
						echo form_label('Usuario', 'usuario');
						echo form_input(['class' => 'form-control' , 'name' => 'usuario']);
						echo '</div>';
						echo form_submit(['class' => 'btn btn-primary' , 'name' => 'enviar' , 'value' => 'Crear Usuario']);
						
						echo form_close();
									?>
					<!-- <form action="usuario" method="POST">
						<div class="form-group">
							<label for="nombre">Nombre</label>
							<input type="text" class="form-control" name="nombre">
						
							
							<br>
							<label for="usuario">Usuario</label>
							<input type="text" class="form-control" name="usuario">
						</div>
						<button type="submit" name="btn btn-primary" class="btn btn-primary">Submit</button>
					</form> -->
				</div>
			</div>
			<div class="row" style="height: 3vh"></div>
			<hr>
			<div class="row" style="height: 3vh"></div>
			<div class="row">
				<?php
					if (empty($users)) {
						echo '<h3>No hay datos</h3>';
					} else {
						echo '<div class="col-5"></div><h3 style="text-align: center;">Tabla de usuarios</h3>';
						echo '<table class="table">
								<thead>
									<tr>
										<th scope="col">ID</th>
										<th scope="col">Nombre</th>
										<th scope="col">Usuario</th>
										
									</tr>
								</thead>
							<tbody>';
						foreach ($users as $us) {
							echo '<tr>
									<td>'.$us->id.'</td>
									<td>'.$us->nombre.'</td>
									<td>'.$us->usuario.'</td>
								</tr>';
						}
						echo '</tbody>
						</table>';
					}
					
				?>
			</div>
		</div>
	</div>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</body>
</html>