<?php
    $Servidor="localhost";
    $usuario="root";
    $clave="";
    $db="prueba";

    $conectar=mysqli_connect($Servidor,$usuario,$clave,$db);

    if(!$conectar){
        echo("No se pudo conectar con el servidor");
    }

    if(isset($_POST['enviar'])){
        $nombre=$_POST['nombre'];
        $apellido=$_POST['apellido'];
        $tel=$_POST['tel'];
        $email=$_POST['email'];

        $sql="INSERT INTO datos VALUES('$nombre','$apellido','$tel','$email')";

        $ejecutar=mysqli_query($conectar, $sql);

        if (!$ejecutar) {
            echo("Hubo algún error al guardar datos");
        }
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <meta http-equiv="refresh" content="60" />
</head>
<body id="cuerpo" class="container" style="background-color: lightgray; background-image: url('https://i.pinimg.com/originals/9b/c9/8f/9bc98f55db5737c6b9601ea215f14467.jpg'); background-repeat: no-repeat; background-size: cover;">
     <div style="display: flex; flex-direction: row" >   
        <div>
            <br>
            <div style="background-color: white; width: 45vw;  border-radius: 40px;">
            <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']) ?>" method="POST" style="width: 40vw; ">
                <div style="padding-left: 5vh; padding-top: 5vh; padding-bottom: 3vh">
                    Nombre: <input type="text" name="nombre"><br>
                    Apellido: <input type="text" name="apellido"><br>
                    Telefono: <input type="text" name="tel"><br>
                    Correo: <input type="text" name="email"><br><br>
                </div>
                    <input type="submit" name="enviar" value="Enviar" class="btn-large blue darken-4" style="width: 45vw; border-radius: 0px 0px 40px 40px;">
            </form>
            </div>
            <br>
            <div style="padding: 5vh; background-color: white; width: 45vw; border-radius: 40px">
            <table style="width: 40vw;" class="striped centered responsive-table highlight" >
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Telefono</th>
                        <th>Correo</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $consultarBase = "SELECT * FROM `datos`";
                        $ejecutarConsulta = mysqli_query($conectar, $consultarBase);
                        $verFilas = mysqli_num_rows($ejecutarConsulta);
                        $fila = mysqli_fetch_array($ejecutarConsulta);

                        if(!$ejecutarConsulta){
                            echo("Hubo un error al realizar la consulta");
                        }else{
                            if($verFilas<1){
                            echo("<tr><td>Sin registros</td></tr>");
                            }else{
                                for($i=0; $i<=$fila; $i++){
                                    echo('
                                        <tr>
                                            <td>'.$fila[0].'</td>
                                            <td>'.$fila[1].'</td>
                                            <td>'.$fila[2].'</td>
                                            <td>'.$fila[3].'</td>
                                        </tr>
                                    ');
                                    $fila = mysqli_fetch_array($ejecutarConsulta);
                                }
                            }
                        }
                    ?>
                </tbody>
            </table>
            </div>    
        </div>
        <br>
        <div style="padding:3vh">
            <div style="background-color: white; width: 35vw; height: 70vh; border-radius: 40px; padding: 5vh">
                <div id="tiempo">
                    <h3>
                        <?php 
                            $time = time(); 
                            echo date("d/m/Y h:i a e", $time);
                        ?>
                    </h3>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<?php
    sqli_close($conectar);
?>
<script>
    $(document).ready(function(){
        setInterval(
            function(){
                $('#cuerpo').load('fecha.php');
            },1000
          );  
        });	
</script>
