<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script
        src="https://code.jquery.com/jquery-3.5.1.js"
        integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
        crossorigin="anonymous">
    </script>
</head>
<body>
    <div id="fecha">
        <h1><?php 
            $time = time();
            echo date("d/m/Y (h:i:s a) e", $time);
        ?></h1>
    </div>
</body>
</html>
<script>
    $(document).ready(function(){
        setInterval(
            function(){
                $('#fecha').load('fecha.php');
            },1000
          );  
        });	
</script>