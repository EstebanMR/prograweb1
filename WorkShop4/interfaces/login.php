<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container">
    <div class="row">
        <div class="col-4"></div>
        <div class="col-4" style="height: 100vh;">
            <div class="d-flex flex-column">
                <div class="p-2" style="height: 30vh;"></div>
                <div class="p-3 pl-4 d-flex justify-content-center" style="height: 39vh; border: black 1px solid; border-radius: 50px;">
                    <form action="" method="POST">
                        <h3 class="pl-5">Iniciar sesión</h3>
                        <br>
                        <div>
                            <label>Nombre de usuario</label>
                            <input type="text" name="nombre" style="width: 18vw;" placeholder="Nombre de usuario">
                        </div>
                        <div>
                            <label>Contraseña</label>
                            <br>
                            <input type="password" name="contra" style="width: 18vw;" placeholder="Contraseña">
                        </div>
                        <br>
                        <input type="submit" class="btn large btn-primary" value="Ingresar" name="enviar" style="width: 18vw;">
                    </form>
                </div>
                <div class="p-2" style="height: 30vh;">
                    <div style="color: red;">
                        <?php
                            if(isset($errorLogin)){
                                echo $errorLogin;
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-4">
        </div>
    </div>
    </div>
</body>
</html>
