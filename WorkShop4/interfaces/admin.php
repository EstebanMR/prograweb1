<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</head>
<body>
    
    <form method="POST">
        <input type="submit" class="btn btn-primary" value="Cerrar sesion" name="salir"></input>
    </form>
    
    <br>
    <br>
    <div>
        <h1>Bienvenido admin <?php echo $usuario->nombre ?></h1>
    </div>
    <div class="container">
    <br>
    <br>
    <h1>Creación de usuarios</h1>
    <form  action="ingresar.php" method="POST" >
      <div class="form-group">
        <label>Nombre</label>
        <input class="form-control" type="text" name="nombre1" placeholder="Nombre ">
      </div>
      <div class="form-group"> 
        <label>Apellidos</label>
        <input class="form-control" type="text" name="apellidos1" placeholder="Apellidos">
      </div>
      <div class="form-group">
        <label>Nombre de usuario</label>
        <input class="form-control" type="text" name="nu1" placeholder="Nombre de usuario">
      </div>
      <div class="form-group">
        <label>Contraseña</label>
        <input class="form-control" type="password" name="pas1" placeholder="Contraseña">
      </div>

      <input type="submit" class="btn btn-primary" value="Enviar" name="enviar1"></input>
    </form>
</div>
</body>
</html>
<?php
    if(isset($_POST['salir'])){
        $sesion->cerrarSesion();
    }
?>