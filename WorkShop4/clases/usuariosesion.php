<?php
    class usuarioSesion{
        public function _constructor()
        {
            session_start();
        }

        public function cambiarUsuarioActual($usuario)
        {
            $_SESSION['usuario'] = $usuario;
        }

        public function darUsuarioActual()
        {
            return $_SESSION['usuario'];
        }
        public function cerrarSesion()
        {
            session_unset();
            session_destroy();
            header('location: ../index');
        }
    }
?>