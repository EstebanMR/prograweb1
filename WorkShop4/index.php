
<?php
    include_once 'clases/usuario.php';
    include_once 'clases/usuariosesion.php';
    
    $sesion = new usuarioSesion();
    $usuario = new usuario();

    $sesion->_constructor();

    if(isset($_SESSION['usuario'])){
        
        $array = $sesion->darUsuarioActual();
        $usuario->nombre = $array[0];
        $usuario->apellido = $array[1];
        $usuario->nusuario = $array[2];
        $usuario->pas = $array[3];
        $usuario->admin = $array[4];
        $usuario->us = $array;
        
        if($usuario->admin=="1"){
            include_once 'interfaces/admin.php';
        }else{
            include_once 'interfaces/usuario.php';
        }

    }else if (isset($_POST['nombre'])&& isset($_POST['contra'])) {

        $nu = $_POST['nombre'];
        $pas = $_POST['contra'];

        if($usuario->userExist($nu, $pas)){

            try{
                $usuario->setUsuario($nu, $pas);
                $sesion->cambiarUsuarioActual($usuario->us);

                if($usuario->admin=="1"){
                    include_once 'interfaces/admin.php';
                }else{
                    include_once 'interfaces/usuario.php';
                }
            } catch(Exception $error){
                print_r('Error: ' . $e->getMessage());
            }

        }else{
            $errorLogin="Datos incorrectos, intentelo de nuevo";
            include_once 'interfaces/login.php';
        }

    }else if(!isset($_SESSION['usuario'])){

        include_once 'interfaces/login.php';
    }
    
?>
                
