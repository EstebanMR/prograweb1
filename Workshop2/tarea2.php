<?php
 $paises = array( "Italy"=>"Rome", "Luxembourg"=>"Luxembourg", "Belgium"=> "Brussels", "Denmark"=>"Copenhagen", 
 "Finland"=>"Helsinki", "France" => "Paris", "Slovakia"=>"Bratislava", "Slovenia"=>"Ljubljana", "Germany" => "Berlin", 
 "Greece" => "Athens", "Ireland"=>"Dublin", "Netherlands"=>"Amsterdam", "Portugal"=>"Lisbon", "Spain"=>"Madrid", 
 "Sweden"=>"Stockholm", "United Kingdom"=>"London", "Cyprus"=>"Nicosia", "Lithuania"=>"Vilnius", "Czech Republic"=>"Prague", 
 "Estonia"=>"Tallin", "Hungary"=>"Budapest", "Latvia"=>"Riga", "Malta"=>"Valetta", "Austria" => "Vienna", "Poland"=>"Warsaw") ;
 ksort($paises);

$temp = array(78, 60, 62, 68, 71, 68, 73, 85, 66, 64, 76, 63, 75, 76, 73, 68, 62, 73, 72, 65, 74, 62, 62, 65, 64, 68, 73, 75, 79, 73);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
</head>
<body class="container" style="background-color: lightgray;">
    <div style="display: flex; flex-direction: column">
        <div>
            <br>
            <br>
            <div style="padding: 3vw; background-color: white;  border-radius: 50px;">
                <table class="striped centered responsive-table highlight" >
                    <thead>
                        <tr>
                            <th colspan="5">
                                <h3>Countries and its capitals</h3>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $n=0;
                            foreach($paises as $pais=>$capital){
                                $n++;
                                if($n==1){
                                    echo('<tr><td style="width: 24vw;">The capital of '.$pais.' is '.$capital.'</td>');
                                }elseif($n>1 && $n<5){
                                    echo('<td style="width: 24vw;">The capital of '.$pais.' is '.$capital.'</td>');
                                }else if($n==5){
                                    echo('<td style="width: 24vw;">The capital of '.$pais.' is '.$capital.'</td></tr>');
                                    $n=0;
                                }
                            }
                        ?>
                    </tbody>
                </table>
            </div>
            <br>
            <br>
        </div>
        
            <br>
            <br>
            <div style="padding: 3vw; background-color: white; border-radius: 50px;">
            <table class="striped responsive-table highlight" >
                    <tbody>
                        <?php
                            sort($temp);
                            $avg = array_sum($temp)/count($temp);
                            echo"<h3>Temperatures</h3>";
                            echo"<br>";
                            echo"<br>";
                            echo "Average Temperature is : ".(round($avg * 10) / 10)."°F";
                            echo"<br>";
                            echo"<br>";
                            $temp=array_unique($temp);

                            $final=count($temp)-5;
                            /*foreach($temp as $t){
                                echo"<span>".$t."</span><br>";
                            }*/
                            echo"List of five lowest temperatures : ";
                            $cont=0;
                           
                            foreach($temp as $t){
                                    
                                $cont++;
                                    if($cont<=4){    
                                        echo $t;
                                        echo ", ";
                                    } elseif($cont==5){
                                        echo $t;
                                        echo ". ";
                                        echo"<br>";
                                        echo"<br>";
                                        echo"List of highest lowest temperatures : ";
                                    } elseif ($cont > $final && $cont < count($temp)){
                                        echo $t;
                                        echo ", ";
                                    }  elseif($cont == count($temp)){
                                        echo $t;
                                        echo ". ";
                                    }                                            
                            }


                        ?>
                    </tbody>
                </table>
            
        </div>
    </div> 
</body>
</html>