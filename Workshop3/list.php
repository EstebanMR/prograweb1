<?php
    $servidor="localhost";
    $usuario="root";
    $clave="";
    $db="tarea3";
    try{
        $conectar=mysqli_connect($servidor,$usuario,$clave,$db);

        if(!$conectar){
            $mensaje = "Falló la conexión a la base de datos";
        }else{
            $script="SELECT * FROM `datos`";
            try{
                $ejecucion=mysqli_query($conectar, $script);
                $categorias= $ejecucion->fetch_all();
            }catch(Exception $error){
                echo'Ha ocurrido un error =>', $error;
            }
                    
        }
    } catch(Exception $error){
        echo'Ha ocurrido un error =>', $error;
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <title>Document</title>
</head>
<body class="container">
    <br>
    <br>
    <a href="/Workshop3/" class="btn-floating btn"><i class="material-icons">arrow_back</i></a>
    <br>
    <h4>Lista de categorías</4>
    <br>
    <br>
    <table>
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Descripción</th>
                <th style="display:none">ID</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php
                foreach($categorias as $cat) {
                echo "<tr><td>".$cat[0]."</td><td>".$cat[1]."</td><td style='display:none'>".$cat[2]."</td><td><a  href=\"/Workshop3/editar.php?id=".$cat[2]."&name=".$cat[0]."&description=".$cat[1]."\">Edit</a> | <a href=\"/Workshop3/eliminar.php?id=".$cat[2]."\">Delete</a></td></tr>";
                }
            ?>
        </tbody>
    </table>
</body>
</html>