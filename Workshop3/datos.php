<?php
  $mensaje = "";
  if(!empty($_REQUEST['status'])) {
    $mensaje = $_REQUEST['message'];
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <title>Document</title>
</head>
<body>
<div class="container">
    <br>
    <br>
    <a href="/Workshop3/" class="btn-floating btn"><i class="material-icons">arrow_back</i></a>
    <br>
    <h1>Ingreso de categorías</h1>
    <form action="conexion.php" method="POST" >
      <div>
        <label>Nombre de categoría</label>
        <input type="text" name="nombre" placeholder="Nombre de categoría">
      </div>
      <div>
        <label>Descripción</label>
        <input type="text" name="descripcion" placeholder="Descripción">
      </div>

      <input type="submit" class="btn btn-primary" value="Enviar" name="enviar"></input>
    </form>
</div>
<div >
    <?php 
        echo ('<br>
        <div class="container">
            <h5>'.$mensaje.'</h5>
        </div>');
    ?>
</div>

</body>
</html>