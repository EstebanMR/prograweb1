<?php
    $id = $_REQUEST['id'];
    $nombre = $_REQUEST['name'];
    $descripcion = $_REQUEST['description'];
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <title>Document</title>
</head>
<body>
    <div class="container">
        <br>
        <br>
        <a href="/Workshop3/list.php" class="btn-floating btn"><i class="material-icons">arrow_back</i></a>
        <br>
        <h1>Editor de categorías</h1>
        <form action="editor.php" method="POST" >
        <div style="display:none">
            <label>id</label>
            <input type="text" name="id" value="<?php echo $id;?>" placeholder="id">
        </div>
        <div>
            <label>Nombre de categoría</label>
            <input type="text" name="nombre" value="<?php echo $nombre;?>" placeholder="Nombre de categoría">
        </div>
        <div>
            <label>Descripción</label>
            <input type="text" name="descripcion" value="<?php echo $descripcion;?>" placeholder="Descripción">
        </div>

        <input type="submit" class="btn btn-primary" value="Enviar" name="enviar"></input>
        </form>
    </div>
</body>
</html>
